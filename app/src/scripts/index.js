import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const baseApiUrl = 'https://jsonplaceholder.typicode.com/';

/**
 * Cached elements
 */
let $posts;
let $card;

/**
 * Variables
 */
let post;
let card;

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function () {
    $posts = $("#app");
    $card = $("#card");
    
    /**
     * When you click a post title
     */
    $posts.click(function(event) {
        console.log(event.target.value);
        $("#app").hide();
        
        singlePostQuery(event.target.value);
    });
    
    /**
     * Back button event
     */
    $card.click(function(event) {
        $("#app").show();
        $card.empty();
    });
    
    // Populate the post list when you open the app
    fetchAllPostTitles();
})

/**
 * Fetches all posts and runs makeTitleList()
 */
function fetchAllPostTitles() {
    queryAndDo("posts", makeTitleList);
}

/**
 *
 * @param query to API
 * @param doFunction specifies what you want to have done with your data
 */
function queryAndDo(query, doFunction) {
    $.ajax({
        url: baseApiUrl + query,
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function(request) {
            console.log("beforeSend()", request);
        },
        success: function(data) {
            console.log("success()", data);
            doFunction(data);
        },
        error: function(response) {
            console.log("complete()", response);
        },
    }).done(function(data) {
        console.log("done()", data);
    });
}

function singlePostQuery(id) {
    $.ajax({
        url: baseApiUrl + "posts/" + id,
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function(request) {
            console.log("beforeSend()", request);
        },
        success: function(data) {
            console.log("success()", data);
            post = new Post(data.id, data.userId, data.title, data.body);
            userQuery(data.userId);
        },
        error: function(response) {
            console.log("complete()", response);
        },
    }).done(function(data) {
        console.log("done()", data);
    });
}

function userQuery(userId) {
    $.ajax({
        url: baseApiUrl + "users/" + userId,
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        beforeSend: function(request) {
            console.log("beforeSend()", request);
        },
        success: function(data) {
            console.log("success()", data);
            card = createCard(null, post.getTitle(), post.getBody(), data.name, data.username, data.email);
            $card.append(card);
            $card.append(`<button type="button" id="backButton" class="btn btn-danger">Back</button>`);
        },
        error: function(response) {
            console.log("complete()", response);
        },
    }).done(function(data) {
        console.log("done()", data);
    });
}

/**
 * Makes a list of buttons with all blog titles
 * @param {*} posts 
 */
function makeTitleList(posts) {
    $posts.append(`<div class="list-group">`);
    $.each(posts, (i, post) => {
        let tempObject = new Post(post.id, post.userId, post.title, post.body);
        $posts.append(`<button value=${post.id} class="list-group-item list-group-item-action">${post.title}</button>`);
    });
    $posts.append(`</div class=list-group>`);
}

/**
 * Class with constructor to create Post objects
 */
class Post {
    constructor(id, userId, title, body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    getTitle() {
        return this.title;
    }

    getBody() {
        return this.body;
    }

    getId() {
        return this.id;
    }
}

/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
function createCard(url, title, body, name, username, email) {
    return `
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                ${url
                    ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}

                <div class="card-body">
                    <h5>${title}</h5>
                    ${body.split('\n').map(para => `<p>${para}</p>`).join('\n')}
                    <p><b>Name:</b> ${name}</p>
                    <p><b>Username:</b> ${username}</p>
                    <p><b>Email:</b> ${email}</p>
                </div>
            </div>
        </div>
    `;
}